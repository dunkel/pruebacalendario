/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.5.50-0ubuntu0.14.04.1-log : Database - examen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`examen` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `examen`;

/*Table structure for table `contacto` */

DROP TABLE IF EXISTS `contacto`;

CREATE TABLE `contacto` (
  `id_contacto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_contacto`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `contacto` */

insert  into `contacto`(`id_contacto`,`nombre`,`correo`,`direccion`) values (1,'julio','dunkel015@hotmail.com','zacatepetl mz 66 lt 36 c la florida cd  azteca');

/*Table structure for table `telefonos` */

DROP TABLE IF EXISTS `telefonos`;

CREATE TABLE `telefonos` (
  `id_telefono` int(11) NOT NULL AUTO_INCREMENT,
  `contacto_id` int(11) NOT NULL,
  `numero` varbinary(12) DEFAULT NULL,
  PRIMARY KEY (`id_telefono`),
  KEY `contacto_id` (`contacto_id`),
  CONSTRAINT `telefonos_ibfk_1` FOREIGN KEY (`contacto_id`) REFERENCES `contacto` (`id_contacto`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `telefonos` */

insert  into `telefonos`(`id_telefono`,`contacto_id`,`numero`) values (1,1,'57745305'),(2,1,'5565768812');

/* Procedure structure for procedure `contactos` */

/*!50003 DROP PROCEDURE IF EXISTS  `contactos` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `contactos`(in in_nombre char(255),in in_correo char(255),in in_direccion char(255))
BEGIN
	declare id int DEFAULT 0;
	select id_contacto into id from contacto where nombre = in_nombre;
	if id < 1 then
	 insert into contacto (nombre,correo,direccion) value (in_nombre,in_correo,in_direccion);
	 set id = last_insert_id();
	 
	END IF;
	SELECT * from contacto where id_contacto= id;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
