/**
 * Created by dunkel on 31/07/16.
 */
/**
 * Created by dunkel on 31/07/16.
 */
$(document).ready(function () {

    $("#form_telefono").validate({
        rules: {
            nombre: "required",
            correo: {
                required: true,
                email: true
            },
            direccion: "required",
            telefono: {
                required: true,
                number: true
            },
        },
        messages: {
            nombre: "Por favor, introduce un nombre",
            direccion: "Por favor, introduce una direcci&oacute;n",
            correo: {
                required: "Por favor, introduce un correo",
                email: "Por favor, introduce un correo valida",
            },
            telefono: {
                required: "Por favor, introduce un tel&eacute;fono",
                number: "Por favor, introduce solo n&uacute;meros",
            },
        }
    });
    $('#editar').on('click',function () {
        if ($('#form_telefono').valid()) {
            var data= $('#form_telefono').serialize();
            console.log(data);
            $.post( "/contacto/editar",data, function( data ) {
                $('#myModal').modal('show');
                if(data.code==200){
                    $('.modal-body').html('El registro se a guardado correctamente');
                }else{
                    $('.modal-body').html('Error al guardar el registro');
                }
            }, "json");
        } else {
            console.log('form is not valid');
        }
    });
    $('#myModal').on('hidden', function () {
        $('.modal-body').html('');
    });
});