/**
 * Created by dunkel on 31/07/16.
 */
$(document).ready(function () {
    var input = 1;
    $('#agregar_tel').on('click', function () {
        html = '<div class="control-group">' +
            '<label class="control-label" for="telefono-'+input+'">Tel&eacute;fono</label>' +
            '<div class="controls">' +
            '<input type="text" id="telefono-'+input+'" name="telefono-'+input+'" placeholder="Direcci&oacute;n"  >' +
            '</div>' +
            '</div>';
        $('.form-horizontal').append(html);
        $( "#telefono-"+input).rules( "add", {
            required: true,
            number:true,
            messages: {
                required: "Por favor, introduce un telefono",
                number: "Por favor, introduce solo n&uacute;meros",
            }
        });
        $('#total').val(input);
        input++;
    });
    $("#gurardar_contacto").validate({
        rules: {
            nombre: "required",
            correo: {
                required: true,
                email: true
            },
            direccion: "required",
            "telefono-0": {
                required: true,
                number: true
            }
        },
        messages: {
            nombre: "Por favor, introduce un nombre",
            direccion: "Por favor, introduce una direcci&oacute;n",
            correo: {
                required: "Por favor, introduce un correo",
                email: "Por favor, introduce un correo valida",
            },
            "telefono-0":{
                required: "Por favor, introduce un tel&eacute;fono",
                number: "Por favor, introduce solo n&uacute;meros",
            }

        }
    });
    $('#guardar').on('click',function () {
        if ($('#gurardar_contacto').valid()) {
            var data= $('#gurardar_contacto').serialize();
            $.post( "contacto/contacto",data, function( data ) {
                if(data.code==200){
                    $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×' +
                        '</button><h3 id="myModalLabel">Agregar Contacto</h3></div><div class="modal-body">El registro se a guardado correctamente' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Cerrar</button>' +
                        '</div>');
                }else{
                    $('#myModal').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×' +
                        '</button><h3 id="myModalLabel">Agregar Contacto</h3></div><div class="modal-body">Error al guardar el registro' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Cerrar</button>' +
                        '</div>');
                }
            }, "json");
        } else {
            console.log('form is not valid');
        }
    });
});