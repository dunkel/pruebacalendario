<?php
/**
 * Created by PhpStorm.
 * User: dunkel
 * Date: 30/07/16
 * Time: 01:29 AM
 */

$route = new Zend_Controller_Router_Route(
    'index',
    array(
        'controller' => 'index',
        'action'     => 'index'
    )
);

$router->addRoute('index', $route);

$route = new Zend_Controller_Router_Route(
    'prueba',
    array(
        'controller' => 'index',
        'action'     => 'prueba'
    )
);

$router->addRoute('prueba', $route);

$route = new Zend_Controller_Router_Route(
    'detalle/:id',
    array(
        'controller' => 'telefono',
        'action'     => 'detalle',
    )
);

$router->addRoute('detalle', $route);