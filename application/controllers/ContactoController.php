<?php

/**
 * Class ContactoController
 */
class ContactoController extends Zend_Controller_Action
{

    /**
     * @var
     */
    protected $model_contacto;

    /**
     *
     */
    public function init()
    {
        /* Initialize action controller here */
        $this->model_contacto = new Application_Model_Contacto();
    }


    /**
     *
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getParams();
            $list = $this->model_contacto->Collection($post);
            $total = $this->model_contacto->RowCount();
            $sEcho = isset($post['sEcho']) ? ($post['sEcho']) : "0";
            $output = array("sEcho" => intval($sEcho), "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => array());
            if (count($total) > 0) {
                foreach ($list as $column => $value) {
                    $value = (object)$value;
                    $row["DT_RowId"] = "row-" . $value->id_contacto;
                    $row['nombre'] = $value->nombre;
                    $row['numero'] = $value->numero;
                    $row['correo'] = $value->correo;
                    $row['direccion'] = $value->direccion;
                    $row['detalle'] = '<a href="detalle/' . $value->id_telefono . '">detalle</a>';
                    array_push($output['aaData'], $row);
                }
                $output['iTotalDisplayRecords'] = $total;
                $output['iTotalRecords'] = $total;
            }
            header('Content-type: application/json');
            echo Zend_Json::encode(
                $output,
                false,
                array('enableJsonExprFinder' => true)
            );
        }
    }

    /**
     *
     */
    public function contactoAction()
    {
        $request = $this->getRequest();
        $output = array();
        if ($request->isPost()) {
            $post = $request->getParams();
            $result = $this->model_contacto->AddContacto($post);
            $output['code'] = $result;
            $output['msj'] = 'Error al guardar el registro';
            if ($result == Application_Model_Contacto::CONT_SUCCESS) {
                $output['msj'] = 'registro guardado correctamente';
            } else if ($result == Application_Model_Contacto::CONT_VACIO) {
                $output['msj'] = 'favor de llenar los campor requeridos';
            }
            //header('Content-type: application/json');
            echo Zend_Json::encode(
                $output,
                false,
                array('enableJsonExprFinder' => true)
            );
        }
    }

    /**
     *
     */
    public function formularioAction()
    {

    }

    /**
     *
     */
    public function editarAction()
    {

        $request = $this->getRequest();
        $output = array();
        if ($request->isPost()) {
            $post = $request->getParams();
            try {
                if (empty($post['id']) || empty($post['telefono']) || empty($post['id_contacto']) || empty($post['nombre']) || empty($post['direccion']) || empty($post['telefono'])) {
                    $output['code'] = Application_Model_Contacto::CONT_ERROR;
                    $output['msj'] = 'Favor de llenar los campos obligatorios';
                } else {

                    $this->model_contacto->getAdapter()->beginTransaction();

                    $this->model_contacto->setCorreo($post['correo']);
                    $this->model_contacto->setNombre($post['nombre']);
                    $this->model_contacto->setIdContacto($post['id_contacto']);
                    $this->model_contacto->setDireccion($post['direccion']);
                    $this->model_contacto->update($post);


                    $output['code'] = Application_Model_Contacto::CONT_SUCCESS;
                    $output['msj'] = 'El registro se actualizo correctamente';
                    $this->model_contacto->getAdapter()->commit();
                }
            } catch (ErrorException $e) {
                $output['code'] = Application_Model_Contacto::CONT_ERROR;
                $output['msj'] = 'Error al guardar el registro';
                $this->model_contacto->getAdapter()->rollBack();
            }

        }
        header('Content-type: application/json');
        echo Zend_Json::encode(
            $output,
            false,
            array('enableJsonExprFinder' => true)
        );

    }
}

