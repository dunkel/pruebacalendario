<?php

/**
 * Class TelefonoController
 */
class TelefonoController extends Zend_Controller_Action
{
    /**
     * @var
     */
    protected $model_telefono;

    /**
     *
     */
    public function init()
    {
        /* Initialize action controller here */
        $this->model_telefono = new Application_Model_Telefonos();
    }

    /**
     *
     */
    public function indexAction()
    {
        // action body
    }

    /**
     *
     */
    public function detalleAction()
    {
        $request = $this->getRequest();
        $get = $request->getParams();
        $this->model_telefono->setIdTelefono($get['id']);
        $result = $this->model_telefono->findId();
        if(isset($result[0])){
            $this->view->id = $result[0]['id_telefono'];
            $this->view->id_contacto = $result[0]['id_contacto'];
            $this->view->nombre = $result[0]['nombre'];
            $this->view->direccion = $result[0]['direccion'];
            $this->view->correo = $result[0]['correo'];
            $this->view->numero = $result[0]['numero'];
        }
    }
}

