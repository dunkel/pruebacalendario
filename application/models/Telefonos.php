<?php

/**
 * Class Application_Model_Telefonos
 */
class Application_Model_Telefonos extends Zend_Db_Table_Abstract
{
    /**
     * @var string
     */
    protected $_name = 'telefonos';
    /**
     * @var string
     */
    protected $_primary = 'id_telefono';

    /**
     * @var
     */
    protected $id_telefono;
    /**
     * @var
     */
    protected $contacto_id;
    /**
     * @var
     */
    protected $numero;

    /**
     * @return mixed
     */
    public function getIdTelefono()
    {
        return $this->id_telefono;
    }

    /**
     * @param mixed $id_telefono
     */
    public function setIdTelefono($id_telefono)
    {
        $this->id_telefono = $id_telefono;
    }

    /**
     * @return mixed
     */
    public function getContactoId()
    {
        return $this->contacto_id;
    }

    /**
     * @param mixed $contacto_id
     */
    public function setContactoId($contacto_id)
    {
        $this->contacto_id = $contacto_id;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * save
     */
    public function AddTelefono()
    {
        try {
            if (!empty($this->numero)) {
                $data = array(
                    'id_telefono' => null,
                    'contacto_id' => $this->contacto_id,
                    'numero' => $this->numero
                );
                $this->insert($data);
            }
        } catch (Zend_Db_Adapter_Exception $e) {
            $e->getMessage();
        } catch (Zend_Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function findId()
    {
        $select =$this->select()->from('telefonos')
            ->setIntegrityCheck(false)
            ->joinInner('contacto','contacto.id_contacto=telefonos.contacto_id','*')
            ->where('id_telefono=?',$this->id_telefono);
        $stmt = $select->query();

        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * @param array $post
     * @return int
     */
    public function update($post=array()){
        $data = array(
            'numero'      => $this->numero,
        );

        $where = $this->getAdapter()->quoteInto('id_telefono = ?', $this->id_telefono);

        $contacto = new Application_Model_Contacto();


        return parent::update($data, $where);
    }
}

