<?php
require "Telefonos.php";


/**
 * Class Application_Model_Contacto
 */
class Application_Model_Contacto  extends Zend_Db_Table_Abstract
{
    /**
     *
     */
    const CONT_SUCCESS=200;
    /**
     *
     */
    const CONT_VACIO=201;
    /**
     *
     */
    const CONT_ERROR=202;


    /**
     * @var string
     */
    protected $_name = 'contacto';
    /**
     * @var string
     */
    protected $_primary = 'id_contacto';
    /**
     * @var
     */
    protected $id_contacto;
    /**
     * @var
     */
    protected $nombre;
    /**
     * @var
     */
    protected $correo;
    /**
     * @var
     */
    protected $direccion;

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param mixed $correo
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getIdContacto()
    {
        return $this->id_contacto;
    }

    /**
     * @param mixed $id_contacto
     */
    public function setIdContacto($id_contacto)
    {
        $this->id_contacto = $id_contacto;
    }


    /**
     * @param array $CONTEXT
     * @return array
     */
    public function Collection($CONTEXT = array())
    {
        $dpage		= isset($CONTEXT['iDisplayStart'])	? 	($CONTEXT['iDisplayStart']) 	: "1";
        $by_n		= isset($CONTEXT['iSortCol_0'])		?	($CONTEXT['iSortCol_0']) 		: "0";
        $by		    = isset($CONTEXT["mDataProp_$by_n"])	? 	($CONTEXT["mDataProp_$by_n"]) 	: "id_contacto";
        $order		= isset($CONTEXT['sSortDir_0'])		? 	($CONTEXT['sSortDir_0']) 		: "ASC";
        $tampag		= isset($CONTEXT['iDisplayLength'])	? 	($CONTEXT['iDisplayLength']) 	: "10";
        $page = ($dpage/$tampag)?($dpage/$tampag)+1:1;

        $select =$this->select()->from('contacto')->setIntegrityCheck(false)->joinInner('telefonos','contacto.id_contacto=telefonos.contacto_id',array('numero','id_telefono'))->order("$by $order")->limitPage($page, $tampag);

        /*$sql = $select->__toString();
        echo "$sql\n";*/

        $stmt = $select->query();

        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * @return int
     */
    public function RowCount(){
        $select = $this->select()->from('contacto','count(*) as total')->setIntegrityCheck(false)->joinInner('telefonos','contacto.id_contacto=telefonos.contacto_id');

        $stmt = $select->query();

        $result = $stmt->fetchAll();

        $total = 0;
        if(isset($result[0])) {
             $total = $result[0]['total'];
        }
        return $total;
    }

    /**
     * @param array $data
     */
    public function AddContacto($data=array()){

        $db = $this->getAdapter();
        if(!empty($data['nombre'])) {
            try{
                $this->getAdapter()->beginTransaction();
                $sql = "CALL contactos('".$data['nombre']."','".$data['correo']."','".$data['direccion']."');";
                $stmt = $this->getAdapter()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if(count($result)>0){
                    $this->id_contacto = $result[0]['id_contacto'];
                    $stmt->closeCursor();
                    for($i=$data['total'];$i>=0;$i--)
                    {
                        $telefono = new Application_Model_Telefonos();
                        $telefono->setContactoId($this->id_contacto);
                        $telefono->setNumero($data['telefono-'.$i]);
                        $telefono->addTelefono();
                    }
                    $this->getAdapter()->commit();
                    $resultado= self::CONT_SUCCESS;
                }else{
                    $this->getAdapter()->rollBack();
                    $resultado= self::CONT_ERROR;
                }
            }catch (Exception $e)
            {
                $this->getAdapter()->rollBack();
                $resultado= self::CONT_ERROR;
            }
        }else{
            $resultado= self::CONT_VACIO;
        }
        return $resultado;
    }

    /**
     * @return array
     */
    public function findId()
    {
        $select =$this->select()->from('contacto')->where('id_contacto=?',$this->id_contacto);
        $stmt = $select->query();

        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * @param array $post
     * @return int
     */
    public function update($post=array())
    {
        $data = array(
            'nombre'      => $this->nombre,
            'correo'      => $this->correo,
            'direccion'      => $this->direccion,
        );

        $where = $this->getAdapter()->quoteInto('id_contacto = ?', $this->id_contacto);

        $telefono = new Application_Model_Telefonos();
        $telefono->setNumero($post['telefono']);
        $telefono->setIdTelefono($post['id']);
        $telefono->update();

        return parent::update($data, $where); // TODO: Change the autogenerated stub
    }
}

